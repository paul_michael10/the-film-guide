<?php

require 'Slim/Slim.php';

$app = new Slim();

$app->get('/films', 'getFilms');
$app->get('/films/:id', 'getFilm');
$app->get('/films/search/:query', 'findById');
$app->post('/films', 'addFilm');
$app->put('/films/:id', 'updateFilm');
$app->delete('/films/:id',	'deleteFilm');

$app->run();

function getFilms() {
	$sql = "select * FROM film ORDER BY name";
	try {
		$db = getConnection();
		$stmt = $db->query($sql);  
		$films = $stmt->fetchAll(PDO::FETCH_OBJ);
		$db = null;
		echo '{"film": ' . json_encode($films) . '}';
	} catch(PDOException $e) {
		echo '{"error":{"text":'. $e->getMessage() .'}}'; 
	}
}


function addFilm() {
	$request = Slim::getInstance()->request();
	$film = json_decode($request->getBody());
	$sql = "INSERT INTO film (name, director, plot) VALUES (:name, :director, :plot)";
	try {
		$db = getConnection();
		$stmt = $db->prepare($sql);  
		$stmt->bindParam("name", $film->name);
		$stmt->bindParam("director", $film->director);
		$stmt->bindParam("plot", $film->plot);
		$stmt->execute();
		$film->id = $db->lastInsertId();
		$db = null;
		echo json_encode($film); 
	} catch(PDOException $e) {
		error_log($e->getMessage(), 3, '/var/tmp/php.log');
		echo '{"error":{"text":'. $e->getMessage() .'}}'; 
		
	}
}

function updateFilm($id) {
	$request = Slim::getInstance()->request();
	$body = $request->getBody();
	$film = json_decode($body);
	$sql = "UPDATE film SET name=:name, director=:director, plot=:plot WHERE id=:id";
	try {
		$db = getConnection();
		$stmt = $db->prepare($sql);  
		$stmt->bindParam("name", $film->name);
		$stmt->bindParam("director", $film->director);
		$stmt->bindParam("plot", $film->plot);
		$stmt->bindParam("id", $id);
		$stmt->execute();
		$db = null;
		echo json_encode($film); 
	} catch(PDOException $e) {
		echo '{"error":{"text":'. $e->getMessage() .'}}'; 
	}
}

function deleteFilm($id) {
	$sql = "DELETE FROM film WHERE id=:id";
	try {
		$db = getConnection();
		$stmt = $db->prepare($sql);  
		$stmt->bindParam("id", $id);
		$stmt->execute();
		$db = null;
	} catch(PDOException $e) {
		echo '{"error":{"text":'. $e->getMessage() .'}}'; 
	}
}

function findById($query) {
	$sql = "SELECT * FROM film WHERE UPPER(id) LIKE :query ORDER BY name";
	try {
		$db = getConnection();
		$stmt = $db->prepare($sql);
		$query = "%".$query."%";  
		$stmt->bindParam("query", $query);
		$stmt->execute();
		$films = $stmt->fetchAll(PDO::FETCH_OBJ);
		$db = null;
		echo '{"film": ' . json_encode($films) . '}';
	} catch(PDOException $e) {
		echo '{"error":{"text":'. $e->getMessage() .'}}'; 
	}
}

function getConnection() {
	$dbhost="localhost";
	$dbuser="B00610281";
	$dbpass="yACB8UGm";
	$dbname="b00610281";
	$dbh = new PDO("mysql:host=$dbhost;dbname=$dbname", $dbuser, $dbpass);	
	$dbh->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
	return $dbh;
}

?>